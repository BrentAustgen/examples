import itertools
import random
import sys

l = lambda card: "╔═══\n║   \n║   \n║   \n║{: <3s}\n╚═══".format(card)
r = lambda card: "═══╗\n{: >3s}║\n   ║\n   ║\n   ║\n═══╝".format(card)

suits = ['♠','♦','♥','♣']
numbers = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']

deck = [i + j for i, j in itertools.product(suits, numbers)]
hand = random.sample(deck, 7)

vparts = [l(hand[0]).split("\n")] + [r(card).split("\n") for card in hand]
for i, _ in enumerate(vparts[0]):
    sys.stdout.write("".join(vpart[i] for vpart in vparts) + "\n")
