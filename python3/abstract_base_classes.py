from abc import ABCMeta, abstractmethod


class AbstractEdge(metaclass=ABCMeta):

    @property
    @abstractmethod
    def head(self):
        pass

    @property
    @abstractmethod
    def tail(self):
        pass


class WeightedEdge(AbstractEdge):

    def __init__(self, head, tail, weight):
        self._head = head
        self._tail = tail
        self._weight = weight

    @property
    def head(self):
        return self._head

    @head.setter
    def head(self, head):
        self._head = head

    @property
    def tail(self):
        return self._tail

    @tail.setter
    def tail(self, tail):
        self._tail = tail

    # by not implementing a setter for weight, it becomes immutable except
    # through modification of 'hidden' self._weight
    @property
    def weight(self):
        return self._weight


edge = WeightedEdge("a", "b", 42)

print((edge.head, edge.tail))

edge.head = "c"
edge.tail = "d"

print((edge.head, edge.tail))

# this line will fail
edge.weight = 15
