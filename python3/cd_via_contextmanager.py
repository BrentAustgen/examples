#!/usr/bin/python

from contextlib import contextmanager
import os

@contextmanager
def cd(newdir):
    curdir = os.getcwd()
    try:
        os.chdir(os.path.expanduser(newdir))
        yield
    finally:
        os.chdir(curdir)

# change path as needed
path = '~'
with cd(path):
    print(os.getcwd())
print(os.getcwd())
