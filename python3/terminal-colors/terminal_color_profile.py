import json
import re
from string import Template
from zipfile import ZipFile

import colorful


class TerminalColorProfile:

    _fg_keys = ["p1", "p2"]
    _bg_keys = ["s1", "s2"]
    _cursor_keys = ["u1", "u2"]
    _color_keys = ["k1", "k2", "r1", "r2", "g1", "g2", "y1", "y2",
                   "b1", "b2", "m1", "m2", "c1", "c2", "w1", "w2"]

    class _AsteriskTemplate(Template):
        delimiter = '*'

    _new_profile_template = _AsteriskTemplate("""\
#!/bin/bash

dconf_dir="/org/gnome/terminal/legacy/profiles:"

old_ids=($(dconf list ${dconf_dir}/ \
           | grep ^:| sed -e "s/\///g" -e "s/://g"))
new_id=$(uuidgen)
new_ids=(${old_ids[@]} ${new_id})
new_list=$(printf "'%s', " ${new_ids[@]})
new_list="[${new_list%, }]"

dconf write ${dconf_dir}/list "${new_list}"
dconf write ${dconf_dir}/:${new_id}/visible-name "'*name'"
dconf write ${dconf_dir}/:${new_id}/use-theme-colors "false"
dconf write ${dconf_dir}/:${new_id}/cursor-colors-set "true"
dconf write ${dconf_dir}/:${new_id}/highlight-colors-set "true"
dconf write ${dconf_dir}/:${new_id}/foreground-color "'*p1'"
dconf write ${dconf_dir}/:${new_id}/highlight-foreground-color "'*p2'"
dconf write ${dconf_dir}/:${new_id}/background-color "'*s1'"
dconf write ${dconf_dir}/:${new_id}/highlight-background-color "'*s2'"
dconf write ${dconf_dir}/:${new_id}/cursor-foreground-color "'*u1'"
dconf write ${dconf_dir}/:${new_id}/cursor-background-color "'*u2'"
dconf write ${dconf_dir}/:${new_id}/palette "*palette"
""")

    _existing_profile_template = _AsteriskTemplate("""\
#!/bin/bash

dconf_dir="/org/gnome/terminal/legacy/profiles:"

dconf write ${dconf_dir}/:*profile_id/visible-name "'*name'"
dconf write ${dconf_dir}/:*profile_id/use-theme-colors "false"
dconf write ${dconf_dir}/:${new_id}/cursor-colors-set "true"
dconf write ${dconf_dir}/:${new_id}/highlight-colors-set "true"
dconf write ${dconf_dir}/:*profile_id/foreground-color "'*p1'"
dconf write ${dconf_dir}/:*profile_id/highlight-foreground-color "'*p2'"
dconf write ${dconf_dir}/:*profile_id/background-color "'*s1'"
dconf write ${dconf_dir}/:*profile_id/highlight-background-color "'*s2'"
dconf write ${dconf_dir}/:*profile_id/cursor-foreground-color "'*u1'"
dconf write ${dconf_dir}/:*profile_id/cursor-background-color "'*u2'"
dconf write ${dconf_dir}/:*profile_id/palette "*palette"
""")

    def __init__(self, colordict, name=None):
        self.colordict = colordict
        if name is None:
            self.name = 'unnamed'
        else:
            self.name = name

    def display(self):
        with colorful.with_palette(self.colordict) as palette:
            default = getattr(palette, "p1_on_s1")
            bold = getattr(palette, "bold")
            hpad = "{:^80s}"
            vpad = "{:^4s}"
            for fill in ["", self.name.upper(), ""]:
                print(default(bold(hpad.format(fill))))
            for bg in self._bg_keys + self._color_keys:
                print(default(vpad.format("")), end="")
                for fg in self._fg_keys + self._color_keys:
                    colorizer = getattr(palette, "{}_on_{}".format(fg, bg))
                    print(colorizer("xyz "), end="")
                print(default(vpad.format("")))
            print(default(hpad.format("")))
            print(default(hpad.format("Press ENTER to continue...")))
            print(default(hpad.format("")), end="")
            input()

    def to_gnome_profile_cmds(self, profile_id=None):
        palette = str([self.colordict[key]
                      for key in self._color_keys])
        kwargs = {**self.colordict, 'name': self.name, 'palette': palette}
        if profile_id is None:
            template = self._new_profile_template
        else:
            template = self._existing_profile_template
            kwargs['profile_id'] = profile_id
        return template.substitute(**kwargs)

    def to_json(self, json_filename):
        with open(json_filename, 'w') as fh:
            json.dump(self.colordict, fh)

    @classmethod
    def from_json(cls, json_filename, name=None):
        with open(json_filename) as fh:
            colordict = json.load(fh)
        return cls(colordict, name)


