#!/usr/bin/python

nostyle = '\033[0m'

for j in range(10):
    for i in [0, 3, 4, 9, 10]:
        if i == 0 or j < 8:
            number = 10 * i + j
            style = '\033[{}m'.format(number)
            print('{:>4} {}abc123{}'.format(number, style, nostyle), end='')
    print()
