import os

from terminal_color_profile import TerminalColorProfile as Profile

for filename in os.listdir('profiles'):
    fullpath = os.path.join('profiles', filename)
    profile = Profile.from_json(fullpath, filename.split('.')[0].upper())
    profile.display()
