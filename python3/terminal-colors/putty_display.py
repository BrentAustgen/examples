import os
import re
import urllib.request
from zipfile import ZipFile

from terminal_color_profile import TerminalColorProfile as Profile


putty_colors_zip_url = 'https://github.com/AlexAkulov/putty-color-themes/'\
                       'archive/master.zip'
putty_colors_zip_filename = os.path.basename(putty_colors_zip_url)

if not os.path.exists(putty_colors_zip_filename):
    urllib.request.urlretrieve(putty_colors_zip_url,
                               putty_colors_zip_filename)

with ZipFile(putty_colors_zip_filename) as zfh:
    regs = [filename
            for filename in zfh.namelist()
            if filename.endswith('.reg')]

# p = primary/fg, s = secondary/bg, u = cursor
# k = black, r = red, g = green, y = yellow
# b = blue, m = magenta, c = cyan, w = white
names = ['p1', 'p2', 's1', 's2', 'u1', 'u2',
         'k1', 'k2', 'r1', 'r2', 'g1', 'g2', 'y1', 'y2',
         'b1', 'b2', 'm1', 'm2', 'c1', 'c2', 'w1', 'w2']

profiles = {}
for reg in regs:
    _, name, _ = os.path.basename(reg).split('.')
    name = name.strip()
    name = 'Symfonic' if name == 'Sympfonic' else name
    with ZipFile(putty_colors_zip_filename) as zfh:
        with zfh.open(reg) as fh:
            colors = re.findall('(\d+),(\d+),(\d+)', fh.read().decode())
            rgbs = ['#{:0>2x}{:0>2x}{:0>2x}'.format(*map(int, color))
                    for color in colors]
    colordict = dict(zip(names, rgbs))
    profiles[name] = Profile(colordict, name)

for name, profile in profiles.items():
    profile.display()
