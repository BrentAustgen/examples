class MyCacheInfo:

    def __init__(self):
        self.hits = 0
        self.misses = 0
        self.storage = dict()

    def __repr__(self):
        out_template = '{}(' + ', '.join(['{}={}'] * 2) + ')'
        out = out_template.format(
            self.__class__.__name__,
            'hits', self.hits,
            'misses', self.misses,
        )
        return out 
