import time
from functools import lru_cache

from mycacheinfo import MyCacheInfo


def knapsack_solver0(values, sizes, capacity):

    def _knapsack(capacity, i):
        cache.misses += 1
        nonlocal values, sizes
        if capacity == 0 or i == 0:
            best = 0
        elif sizes[i-1] > capacity:
            best = _knapsack(capacity, i-1)
        else:
            best = max(_knapsack(capacity-sizes[i-1], i-1) + values[i-1],
                       _knapsack(capacity, i-1))
        return best

    cache = MyCacheInfo()
    obj = 0
    sol = [0] * len(values)
    for i in reversed(range(len(values))):
        sol[i] = int(_knapsack(capacity, i+1) != _knapsack(capacity, i))
        capacity -= sizes[i] * sol[i]
        obj += values[i] * sol[i]
    return obj, sol, cache


def knapsack_solver1(values, sizes, capacity, memoize=True):

    def _knapsack(capacity, i):
        nonlocal values, sizes
        if memoize and (capacity, i) in cache.storage:
            cache.hits += 1
            best = cache.storage[(capacity, i)]
        else:
            cache.misses += 1
            if capacity <= 0 or i <= 0:
                best = 0
            elif sizes[i-1] > capacity:
                best = _knapsack(capacity, i-1)
            else:
                best = max(_knapsack(capacity-sizes[i-1], i-1) + values[i-1],
                           _knapsack(capacity, i-1))
            if memoize:
                cache.storage[(capacity, i)] = best
        return best

    cache = MyCacheInfo()
    obj = 0
    sol = [0] * len(values)
    for i in reversed(range(len(values))):
        sol[i] = int(_knapsack(capacity, i+1) != _knapsack(capacity, i))
        capacity -= sizes[i] * sol[i]
        obj += values[i] * sol[i]
    return obj, sol, cache


def knapsack_solver2(values, sizes, capacity, memoize=True):

    @lru_cache(len(values) * capacity * memoize)
    def _knapsack(capacity, i):
        nonlocal values, sizes
        if capacity == 0 or i == 0:
            best = 0
        elif sizes[i-1] > capacity:
            best = _knapsack(capacity, i-1)
        else:
            best = max(_knapsack(capacity-sizes[i-1], i-1) + values[i-1],
                       _knapsack(capacity, i-1))
        return best

    _knapsack.cache_clear()
    obj = 0
    sol = [0] * len(values)
    for i in reversed(range(len(values))):
        sol[i] = int(_knapsack(capacity, i+1) != _knapsack(capacity, i))
        capacity -= sizes[i] * sol[i]
        obj += values[i] * sol[i]
    return obj, sol, _knapsack.cache_info()


SIZES = [7, 9, 5, 12, 14, 6, 12]
VALUES = [3, 4, 2, 6, 7, 3, 5]
CAPACITY = 20

N = 1000

T0 = time.time()
for _ in range(N):
    res = knapsack_solver0(VALUES, SIZES, CAPACITY)
if N != 1:
    print(time.time() - T0)
print(res)

T0 = time.time()
for _ in range(N):
    res = knapsack_solver1(VALUES, SIZES, CAPACITY, memoize=False)
if N != 1:
    print(time.time() - T0)
print(res)

T0 = time.time()
for _ in range(N):
    res = knapsack_solver1(VALUES, SIZES, CAPACITY, memoize=True)
if N != 1:
    print(time.time() - T0)
print(res)

T0 = time.time()
for _ in range(N):
    res = knapsack_solver2(VALUES, SIZES, CAPACITY, memoize=False)
if N != 1:
    print(time.time() - T0)
print(res)

T0 = time.time()
for _ in range(N):
    res = knapsack_solver2(VALUES, SIZES, CAPACITY, memoize=True)
if N != 1:
    print(time.time() - T0)
print(res)
