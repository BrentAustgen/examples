"""
This is an example of using dynamic programming to solve an LCS (longest common
subsequence) problem.

This problem is detailed in Avrim Blum's lecture notes pertaining to dynamic
programming. This particular implementation serves to illustrate the
difference between having memoization and not. Also, the construction of the
LCS is embedded in the recursive method, not computed with post-processing as
described in the lecture notes.
"""

import time
from functools import lru_cache
from itertools import chain

from mycacheinfo import MyCacheInfo


def lcs0(S, T, memoize=True):

    stor = {}
    info = MyCacheInfo()

    def _lcs(S, T, U=""):
        nonlocal stor, memoize, info

        if memoize and (S, T) in stor:
            info.hits += 1
            (r, U) = stor[(S, T)]
        else:
            info.misses += 1
            if not S or not T:
                r = 0
            elif not S[-1] == T[-1]:
                r1, U1 = _lcs(S[:-1], T, U)
                r2, U2 = _lcs(S, T[:-1], U)
                if r1 >= r2:
                    r, U = r1, U1
                else:
                    r, U = r2, U2
            else:
                r, U = _lcs(S[:-1], T[:-1], U)
                r += 1
                U = U + S[-1]

            if memoize:
                stor[(S, T)] = (r, U)

        return r, U

    r, U = _lcs(S, T)
    return r, U, info


def lcs1(S, T, memoize=True):

    maxsize = len(S) * len(T) if memoize else 0

    @lru_cache(maxsize=maxsize)
    def _lcs(S, T, U=""):
        if not S or not T:
            r = 0
        elif not S[-1] == T[-1]:
            r1, U1 = _lcs(S[:-1], T, U)
            r2, U2 = _lcs(S, T[:-1], U)
            if r1 >= r2:
                r, U = r1, U1
            else:
                r, U = r2, U2
        else:
            r, U = _lcs(S[:-1], T[:-1], U)
            r += 1
            U = U + S[-1]

        return r, U

    _lcs.cache_clear()
    r, U = _lcs(S, T)
    return r, U, _lcs.cache_info()



# These are the example strings presented in Blum's notes. The LCS of S and T
# is "ABAD". (Note that the subsequence need not be contiguous).
S = "ABAZDC"
T = "BACBAD"

# Without memoization, a whopping 315 calls to the inner method are required
# to find the LCS.
start = time.time()
for _ in range(10000):
    x, U, c = lcs0(S, T, memoize=False)
print(time.time() - start)
print(x, U, c)

# But with memoization, that quantity is reduced to just 46.
start = time.time()
for _ in range(10000):
    x, U, c = lcs0(S, T, memoize=True)
print(time.time() - start)
print(x, U, c)

# Without memoization, a whopping 315 calls to the inner method are required
# to find the LCS.
start = time.time()
for _ in range(10000):
    x, U, c = lcs1(S, T, memoize=False)
print(time.time() - start)
print(x, U, c)

# But with memoization, that quantity is reduced to just 46.
start = time.time()
for _ in range(10000):
    x, U, c = lcs1(S, T, memoize=True)
print(time.time() - start)
print(x, U, c)
