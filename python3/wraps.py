"""
This example shows how a function name can be masked when it is decorated and
also how this can be overcome.
"""

from functools import wraps


def decorator_foo(f):
    def _decorator():
        pass
    return _decorator


def decorator_bar(f):
    @wraps(f)
    def _decorator():
        pass
    return _decorator


@decorator_foo
def foo():
    pass


@decorator_bar
def bar():
    pass


# this will print '_decorator', the inner function from decorator_foo
print(foo.__name__)
# this will print 'bar' as expected
print(bar.__name__)
