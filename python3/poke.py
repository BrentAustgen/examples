#!/usr/bin/python

# create an empty class
class Foo:
    pass

# create a global function
def bar(_self, val):
    _self.val = val
    return _self.val

# poke global function into class 
Foo.bar = bar

# create an instance of Foo class
foo = Foo()

# verify function was added
print("\n# verify function was added")
print(dir(Foo))

# use poked function on class instance
print("\n# use poked function on class instance")
print(foo.bar(1))

# remove function
del Foo.bar

# verify function was removed
print("\n# verify function was removed")
print(dir(Foo))

print("")
