#include <iostream>
#include <mpi.h>

using namespace std;

int mpi_rank, mpi_size, mpi_root;

int main(int argc, char *argv[])
{

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    mpi_root = 0;

    if (mpi_rank == mpi_root)
    {
        cout << "I am rank " << mpi_rank << "." << endl;

        int * recvdata;
        for(int i = mpi_root + 1; i < mpi_size; i++)
        {
            MPI_Recv(&recvdata, 1, MPI_INT, i, 0, MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);
            cout << "Receiving from rank " << i << "." << endl;
        }
    }
    else
    {
        int * senddata = &mpi_rank;
        MPI_Send(&senddata, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();

}
