# MPI_Send() and MPI_Recv()

## Compile and Execute
```sh
mpigxx example.cpp -std=c++11 -o example
mpirun -n 4 ./example
```

## Flow

Every non-root process sends its rank to the root process (rank 0). While the non-root processes perform this task concurrently, the root process receives and prints the values from each non-root process in serial fashion.
