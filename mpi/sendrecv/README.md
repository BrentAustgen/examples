# Pairing with MPI_Sendrecv()

## Compile and Execute

```sh
mpigxx example.cpp -std=c++11 -o example
# Try with odd and even number of ranks.
mpirun -n 7 ./example
mpirun -n 8 ./example
```

## Flow

Beginning at rank 0, ranks are paired until there are there are no remaining ranks with which to pair. (If an odd number of processes are specified, the last process does not pair.) Each process then uses MPI_Sendrecv() to send its own rank to its partner and receive and print its partner's rank.

For example, when n is 4:
```
0 <--> 1
2 <--> 3
```

And when n is 5:
```
0 <--> 1
2 <--> 3
4
```
