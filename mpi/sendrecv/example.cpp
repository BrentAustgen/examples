#include <iostream>
#include <mpi.h>

using namespace std;

int mpi_rank, mpi_size, mpi_root;

int main(int argc, char *argv[])
{

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    mpi_root = 0;

    int * recvdata;
    int * senddata = &mpi_rank;

    // The last rank does not participate if mpi_size is odd.
    if (mpi_rank == mpi_size - 1 && mpi_size % 2 == 1)
    {
        cout << "Rank " << mpi_rank << "... odd one out." << endl;
    }
    else
    {
        int other_rank;
        if (mpi_rank % 2 == 0)
            other_rank = mpi_rank + 1;
        else
            other_rank = mpi_rank - 1;

        MPI_Sendrecv(&senddata, 1, MPI_INT, other_rank, 0,
                     &recvdata, 1, MPI_INT, other_rank, 0, MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);

        // TODO: Let mpi_root handle all printing to guarantee consistency.
        cout << "Rank " << mpi_rank << " received from " << other_rank
             << "." << endl;
    }

}
