#include <iostream>
#include <iomanip>
#include <math.h>
#include <algorithm>
#include <vector>
#include <string>
#include <mpi.h>

using namespace std;

int proc, size, root, hold, turn;

void print(vector<int> &data)
{
    int width = ceil(log10(*max_element(data.begin(), data.end()) + 1));
    int N = data.size();
    cout << "{";
    for(int n = 0; n < N - 1; n++)
    {
        cout.width(width);
        cout.flags(ios::right);
        cout << data[n] << ", ";
    }
    cout.width(width);
    cout.flags(ios::right);
    cout << data[N - 1] << "}"<< endl;
}

void print_help()
{
    cout << "Usage: mpirun -n [N] ./sendrecv_rotate [M] [T]\n"
         << "\n"
         << "  N - number of MPI processes, N > 0\n"
         << "  M - size of data each process handles, M > 0\n"
         << "  T - number of positions to turn the data, T >= 0\n"
         << flush;
    exit(1);
}

void parse_input(int argc, char** argv, int &hold, int &turn)
{
    if (argc == 3)
    {
        hold = atoi(argv[1]);
        turn = atoi(argv[2]);
        turn = turn % (hold * size);
        if (turn < 0)
        {
            print_help();
            exit(1);
        }
    }
    else
    {
        print_help();
        exit(1);
    }
}

void do_turn(vector<int> &a_data, vector<int> &z_data)
{
    if (turn == 0)
    {
        z_data = a_data;
        return;
    }

    int num, send_idx, send_proc, recv_idx, recv_proc;

    num       = hold - (turn % hold);
    send_idx  = 0;
    send_proc = (proc + size + turn / hold) % size;
    recv_idx  = (hold - num) % hold;
    recv_proc = (proc + size - turn / hold) % size;

    if (num != 0)
    {
        if (proc == send_proc)
        {
            copy(a_data.begin() + send_idx, a_data.begin() + send_idx + num,
                 z_data.begin() + recv_idx);
        }
        else
        {
            MPI_Sendrecv(&a_data.at(send_idx), num, MPI_INT, send_proc, 0,
                         &z_data.at(recv_idx), num, MPI_INT, recv_proc, 0,
                         MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
    }

    num       = hold - num;
    send_idx  = (hold - num) % hold;
    send_proc = (send_proc + 1) % size;
    recv_idx  = 0;
    recv_proc = (recv_proc + size - 1) % size;

    if (num != 0)
    {
        if (proc == send_proc)
        {
            copy(a_data.begin() + send_idx, a_data.begin() + send_idx + num,
                 z_data.begin() + recv_idx);
        }
        else
        {
            MPI_Sendrecv(&a_data.at(send_idx), num, MPI_INT, send_proc, 0,
                         &z_data.at(recv_idx), num, MPI_INT, recv_proc, 0,
                         MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
    }
}

vector<int> init_data()
{
    int data_size = size * hold;
    vector<int> data(size*hold);
    for(int i = 0; i < data_size; i++)
        data[i] = i;
    return data;
}

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc);
    root = 0;
    parse_input(argc, argv, hold, turn);


    vector<int> beg_data;
    vector<int> end_data(size * hold);
    if (proc == root)
        beg_data = init_data();

    vector<int> beg_proc_data(hold);
    vector<int> end_proc_data(hold);

    MPI_Scatter(&beg_data.front(),      hold, MPI_INT,
                &beg_proc_data.front(), hold, MPI_INT,
                root, MPI_COMM_WORLD);

    do_turn(beg_proc_data, end_proc_data);

    MPI_Gather( &end_proc_data.front(), hold, MPI_INT,
                &end_data.front(),      hold, MPI_INT,
                root, MPI_COMM_WORLD);

    if (proc == root)
    {
        cout << "Before:" << endl;
        print(beg_data);
        cout << "After:" << endl;
        print(end_data);
    }

    MPI_Finalize();
}
