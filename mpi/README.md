# sendrecv_rotate.cpp

This example implements a vector rotation using MPI_Sendrecv().


## Compile and Execute
```
mpigxx sendrecv_rotate.cpp -std=c++11 -o ./sendrecv_rotate
mpirun -n [N] ./sendrecv_rotate [M] [T]
```


## Flow

The root process (rank 0) generates a vector {0, 1, ..., N\*M}. An MPI_Scatter() is called to distribute M pieces of the vector to each of the N processes. In function, all processes form a ring of sorts. Depending on the divisibility of M into T, each process calls MPI_Sendrecv() either once or twice to rotate the data around the ring. When the turn is complete, the root process calls MPI_Gather() to accumulate, then print, the data.
