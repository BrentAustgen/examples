#include <iostream>
#include <iomanip>
#include <algorithm>
#include <math.h>
#include <vector>
#include <mpi.h>

using namespace std;

int mpi_rank, mpi_size, mpi_root;

void print_compare(vector<int> a, vector<int> b)
{
    if (a.size() != b.size())
    {
        cout << "different sizes" << endl;
        return;
    }
    else
    {
        int a_max, b_max, width;
        a_max = *max_element(a.begin(), a.end());
        b_max = *max_element(b.begin(), b.end());
        if (b_max >= a_max)
            width = ceil(log10(b_max + 1));
        else
            width = ceil(log10(a_max + 1));
        cout.flags(ios::right);
        for(int i = 0; i < mpi_size; i++)
        {
            cout << setw(width) << a[i] << " -> "
                 << setw(width) << b[i] << endl;
        }
    }
}

vector<int> init_data(int size)
{
    vector<int> data(size);
    for(int i = 0; i < size; i++)
        data[i] = i;
    return data;
}


int main(int argc, char *argv[])
{

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    mpi_root = 0;

    vector<int> beg_data = init_data(mpi_size);
    vector<int> end_data(mpi_size);
    int proc_data;

    MPI_Scatter(&beg_data.front(), 1, MPI_INT,
                &proc_data,        1, MPI_INT, 0, MPI_COMM_WORLD);

    proc_data *= 2;

    MPI_Gather( &proc_data,        1, MPI_INT,
                &end_data.front(), 1, MPI_INT, 0, MPI_COMM_WORLD);

    if (mpi_rank == 0)
        print_compare(beg_data, end_data);

    MPI_Finalize();
}
