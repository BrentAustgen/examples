# Alter std::vector with MPI_Scatter() and MPI_Gather()

## Compile and Execute
```
mpigxx example.cpp -std=c++11 -o ./example
mpirun -n 4 ./example
```

## Flow
The root process (rank 0) generates a vector {0, 1, ..., mpi_size}. An MPI_Scatter() is invoked to distribute the vector to all processes such that each process assumes ownership of exactly one element. In parallel, each process doubles that value. Finally, an MPI_Gather() is called to collect the doubled data to a second vector on the root process. The root process prints the old values next to the new values.
