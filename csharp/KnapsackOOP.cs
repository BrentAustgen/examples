﻿using System;
using System.Collections.Generic;
using System.Linq;

using Gurobi;

namespace KnapsackOOP
{
    class KnapsackOOP
    {
        static void Main(string[] args)
        {
            GRBEnv env = new GRBEnv();
            GRBModel model = new GRBModel(env);

            List<Item> items = new List<Item>();
            items.Add(new Item( 5, 5, "item0"));
            items.Add(new Item( 8, 6, "item1"));
            items.Add(new Item(10, 2, "item2"));
            items.Add(new Item( 1, 4, "item3"));
            items.Add(new Item( 7, 3, "item4"));

            foreach (Item item in items)
                item.AddVarToModel(model);

            Knapsack knapsack = new Knapsack(11, "knapsack0");
            knapsack.AddConstrToModel(model, items);

            model.ModelSense = GRB.MAXIMIZE;
            model.Optimize();

            foreach (Item item in items)
                Console.WriteLine(item.Var.VarName + ": " + item.Var.X.ToString());
        }
    }

    class Item
    {

        public readonly double Cost;
        public readonly double Size;
        public readonly string Name;
        public GRBVar Var;

        public Item(double cost, double size, string name)
        {
            this.Cost = cost;
            this.Size = size;
            this.Name = name;
        }

        public void AddVarToModel(GRBModel model)
        {
            this.Var = model.AddVar(0, 1, this.Cost, GRB.BINARY, this.Name);
        }

    }

    class Knapsack
    {
        public readonly double Capacity;
        public readonly string Name;
        public GRBConstr conKnapsack;

        public Knapsack(double capacity, string name)
        {
            this.Capacity = capacity;
            this.Name = name;
        }

        public void AddConstrToModel(GRBModel model, List<Item> items)
        {
            GRBLinExpr lhs = 0.0;

            // method #1
            foreach (Item item in items)
                lhs += item.Size * item.Var;

            // method #2
            //double[] coeffs = (from item in items select item.Size).ToArray<double>();
            //GRBVar[] vars = (from item in items select item.Var).ToArray<GRBVar>();
            //lhs.AddTerms(coeffs, vars);

            // method #3
            //lhs += (from item in items select item.Size * item.Var)
            //    .Aggregate<GRBLinExpr, GRBLinExpr>(0.0, (x, y) => x + y);

            this.conKnapsack = model.AddConstr(lhs <= this.Capacity, this.Name);
        }
    }
}
