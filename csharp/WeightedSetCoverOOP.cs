﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Gurobi;

namespace WeightedSetCoverOOP
{
    class WeightedSetCover
    {
        static void Main(string[] args)
        {
            GRBEnv env = new GRBEnv();
            GRBModel model = new GRBModel(env);

            List<CandidateSet> candidates = new List<CandidateSet>();
            candidates.Add(new CandidateSet(new int[] { 1, 2, 1, 0, 0 }, 3, "set0"));
            candidates.Add(new CandidateSet(new int[] { 0, 0, 1, 1, 1 }, 1, "set1"));
            candidates.Add(new CandidateSet(new int[] { 1, 0, 0, 1, 0 }, 2, "set2"));

            foreach (CandidateSet candidate in candidates)
                candidate.AddVarToModel(model);

            CoverSet cover = new CoverSet(new int[] { 8, 7, 6, 5, 4 }, "cover0");
            cover.AddConstrsToModel(model, candidates);

            model.ModelSense = GRB.MINIMIZE;
            model.Optimize();

            foreach (CandidateSet candidate in candidates)
            {
                Console.WriteLine(candidate.Var.VarName + ": " + candidate.Var.X.ToString());
            }
        }
    }

    class CandidateSet
    {
        public readonly int[] Composition;
        public readonly double Weight;
        public readonly string Name;
        public GRBVar Var;

        public CandidateSet(int[] composition, double weight, string name)
        {
            this.Composition = composition;
            this.Weight = weight;
            this.Name = name;
        }

        public void AddVarToModel(GRBModel model)
        {
            this.Var = model.AddVar(0, GRB.INFINITY, this.Weight, GRB.INTEGER, this.Name);
        }

    }

    class CoverSet
    {
        public readonly int[] Composition;
        public readonly string Name;
        public GRBConstr[] Cover;

        public CoverSet(int[] composition, string name)
        {
            this.Composition = composition;
            this.Cover = new GRBConstr[composition.Length];
            this.Name = name;
        }

        public void AddConstrsToModel(GRBModel model, List<CandidateSet> candidates)
        {
            for (int item = 0; item < this.Composition.Length; item++)
            {
                GRBLinExpr lhs = 0;
                foreach (CandidateSet candidate in candidates)
                    lhs += candidate.Composition[item] * candidate.Var;
                Cover[item] = model.AddConstr(lhs >= this.Composition[item], item.ToString());
            }
        }
    }

}
